# Maintainer: Brad Fanella <cesura@archlinux.org>
# Contributor: Martin Wimpress <code@flexion.org>

pkgname=atril
pkgver=1.22.1
pkgrel=2.0
pkgdesc="MATE document viewer"
url="https://mate-desktop.org"
arch=('i686' 'x86_64')
license=('GPL')
depends=('djvulibre' 'libspectre' 'libgxps' 'mathjax2' 'poppler-glib' 'webkit2gtk' 'caja' 'texlive-bin')
makedepends=('gobject-introspection' 'intltool' 'itstool')
groups=('mate-extra')
conflicts=('atril-gtk3')
replaces=('atril-gtk3')
source=("https://pub.mate-desktop.org/releases/${pkgver%.*}/$pkgname-$pkgver.tar.xz"
        0001_prevent_no_doc_segfault.patch
        0002_CVE-2019-1010006.patch)
sha256sums=('208544f6cce4bb5c0bf423a0d618929e35527d1c5ed120a5aa19f2a7a77e5c44'
            'd84cbe0b9b5e59217000d92300986d98b773bfb47b2ca2bfa3c3da52b54fabcb'
            'd2c2cb5b88a8df3e4c31f66ff69ffc77b5da285bf73f0811396242332de72924')

prepare() {
	cd "$pkgname-$pkgver"

	patch -Np1 -i ../0001_prevent_no_doc_segfault.patch
	patch -Np1 -i ../0002_CVE-2019-1010006.patch

	# Fix mathjax path
	sed -i 's|/usr/share/javascript/mathjax|/usr/share/mathjax2|' backend/epub/epub-document.c
}

build() {
	cd "$pkgname-$pkgver"
	./configure \
        	--prefix=/usr \
        	--libexecdir=/usr/lib/${_pkgbase} \
        	--enable-djvu \
        	--enable-dvi \
        	--enable-epub \
        	--enable-t1lib \
        	--enable-pixbuf \
        	--enable-comics \
        	--enable-xps \
        	--enable-introspection

    	#https://bugzilla.gnome.org/show_bug.cgi?id=656231
    	sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool

    	make
}

package() {
    	cd "$pkgname-$pkgver"
    	make DESTDIR="$pkgdir" install
}
